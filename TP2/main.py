from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from pydantic import BaseModel
import mysql.connector

app = FastAPI()

# MySQL connection
mydb = mysql.connector.connect(
    host="imageMySQL",
    user="root",
    password="root",
    database="bdd_mysql",
)
mycursor = mydb.cursor()

# Pydantic model for client
class Client(BaseModel):
    first_name: str
    last_name: str
    email: str
    num_orders: int

# Endpoints for clients

@app.post("/add_client")
def add_client(client_data: Client):
    sql = "INSERT INTO clients (first_name, last_name, email, num_orders) VALUES (%s, %s, %s, %s)"
    val = (client_data.first_name, client_data.last_name, client_data.email, client_data.num_orders)
    mycursor.execute(sql, val)
    mydb.commit()
    return JSONResponse(content={"message": "Client added successfully", "id": mycursor.lastrowid})

@app.put("/update_client/{client_id}")
def update_client(client_id: int, client_data: Client):
    sql = "UPDATE clients SET first_name = %s, last_name = %s, email = %s, num_orders = %s WHERE id = %s"
    val = (client_data.first_name, client_data.last_name, client_data.email, client_data.num_orders, client_id)
    mycursor.execute(sql, val)
    if mycursor.rowcount == 0:
        raise HTTPException(status_code=404, detail="Client not found")
    mydb.commit()
    return JSONResponse(content={"message": "Client updated successfully"})

@app.delete("/delete_client/{client_id}")
def delete_client(client_id: int):
    sql = "DELETE FROM clients WHERE id = %s"
    val = (client_id,)
    mycursor.execute(sql, val)
    if mycursor.rowcount == 0:
        raise HTTPException(status_code=404, detail="Client not found")
    mydb.commit()
    return JSONResponse(content={"message": "Client deleted successfully"})

@app.get("/get_client/{client_id}")
def get_client(client_id: int):
    sql = "SELECT * FROM clients WHERE id = %s"
    val = (client_id,)
    mycursor.execute(sql, val)
    client = mycursor.fetchone()

    if not client:
        raise HTTPException(status_code=404, detail="Client not found")
    return client

# Additional root endpoint
@app.get("/")
async def root():
    return {"message": "Hello World"}